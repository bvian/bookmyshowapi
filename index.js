const express = require('express');
const cors = require("cors");

const errorHandler = require('./middleware/errorHandler');
const db = require("./app/models");
const moviesRoutes = require("./app/routes/movies.routes");
const eventsRoutes = require("./app/routes/events.routes");
const cinemaHallsRoutes = require("./app/routes/cinemaHalls.routes");
const usersRoutes = require('./app/routes/users.routes');
const ordersRoutes = require('./app/routes/orders.routes');

const PORT = process.env.PORT || 8080;

const app = express();

// const corsOptions = {
//     origin: 'http://localhost:3000',
//     credentials: true,            //access-control-allow-credentials:true
//     optionSuccessStatus: 200
// }

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch((err) => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });

app.get("/", (req, res, next) => {
    res.json({
        "message": "Welcome to BookMyShow API Appliction"
    });
});


moviesRoutes(app);
eventsRoutes(app);
cinemaHallsRoutes(app);
usersRoutes(app);
ordersRoutes(app);

app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});