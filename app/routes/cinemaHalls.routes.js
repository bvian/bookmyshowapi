const express = require("express");

const cinemaHalls = require("../controllers/cinemaHall.controller.js");

module.exports = (app) => {

    const router = express.Router();

    router.post("/", cinemaHalls.create);

    router.get("/", cinemaHalls.findAll);

    router.get("/:id", cinemaHalls.findOne);

    router.put("/:id", cinemaHalls.update);

    router.delete("/:id", cinemaHalls.delete);

    router.delete("/", cinemaHalls.deleteAll);

    app.use('/api/cinemahalls', router);
};