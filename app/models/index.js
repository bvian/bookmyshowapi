const mongoose = require("mongoose");

const config = require("../../config/config.js");
const moviesModel = require("./movies.model.js");
const eventsModel = require("./events.model.js");
const cinemaHallsModel = require("./cinemaHalls.model.js");
const usersModel = require("./users.model.js");
const ordersModel = require("./orders.model.js");

mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;
db.url = config.url;

db.movies = moviesModel(mongoose);
db.events = eventsModel(mongoose);
db.cinemahalls = cinemaHallsModel(mongoose);
db.users = usersModel(mongoose);
db.orders = ordersModel(mongoose);

module.exports = db;