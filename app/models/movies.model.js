module.exports = (mongoose) => {
    let schema = mongoose.Schema(
        {
            title: String,
            description: String,
            places: Array,
            type: String,
            date: String,
            language: Array,
            image: String,
            backgroundImage: String,
            rating: Object
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Movie = mongoose.model("movie", schema);
    return Movie;
};