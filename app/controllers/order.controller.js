const db = require("../models");
const Order = db.orders;

exports.create = (req, res, next) => {
    const { userId, title, language, date, time, cinema, seats } = req.body;

    if (!userId || !title || !language || !date || !time || !cinema || !seats ) {
        next({
            status: 400,
            message: "Content can not be empty! required data userId, title, language, date, time, cinema, seats"
        });
        return;
    } else {

        const order = new Order({
            userId, title, language, date, time, cinema, seats
        });

        order
            .save(order)
            .then((data) => {
                res.send(data);
            })
            .catch((err) => {
                next({
                    status: 500,
                    message: err.message || "Some error occurred while creating the Orders."
                });
            });
    }
};

exports.findAll = (req, res, next) => {
    const title = req.query.title;

    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    Order.find(condition)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while retrieving orders."
            });
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    Order.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found Order with id " + id
                });
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error retrieving Order with id=" + id
            });
        });
};

exports.update = (req, res, next) => {
    if (!req.body) {
        return next({
            status: 400,
            message: "Data to update can not be empty!"
        });

    }

    const id = req.params.id;

    Order.findByIdAndUpdate(id, req.body)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot update Order with id=${id}. Maybe Order was not found!`
                });
            } else {
                res.send({ message: "Order was updated successfully." });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error updating Order with id=" + id
            });
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Order.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot delete Order with id=${id}. Maybe Order was not found!`
                });
            } else {
                res.send({
                    message: "Order was deleted successfully!"
                });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Could not delete Order with id=" + id
            });
        });
};

exports.deleteAll = (req, res, next) => {
    Order.deleteMany({})
        .then((data) => {
            res.send({
                message: `${data.deletedCount} Orders were deleted successfully!`
            });
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while removing all orders."
            });
        });
};