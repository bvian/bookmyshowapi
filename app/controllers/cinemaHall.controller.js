const db = require("../models");
const Cinemahall = db.cinemahalls;

exports.create = (req, res, next) => {
    const { title, location, moviesTime, date } = req.body;

    if (!title || !location || !moviesTime || !date) {
        next({
            status: 400,
            message:  "Content can not be empty! required data title, location, movieWithTime, date, seats"
        });
        
        return;
    } else {
        const seats = Object.values(moviesTime)
            .reduce((totalSeats, timeList) => {
                const allSeats = timeList.reduce((sumAllSeats, time) => {
                    const newTimeArray = new Array(100);
                    newTimeArray.fill(false);
                    sumAllSeats = {
                        ...sumAllSeats,
                        [time]: newTimeArray
                    };
                    return sumAllSeats
                }, {});

                totalSeats = {
                    ...totalSeats,
                    ...allSeats
                };

                return totalSeats;
            }, {});

        const cinemahall = new Cinemahall({
            title,
            location,
            moviesTime,
            date,
            seats
        });

        cinemahall
            .save(cinemahall)
            .then((data) => {
                res.send(data);
            })
            .catch((err) => {
                next({
                    status: 500,
                    message: err.message || "Some error occurred while creating the cinema halls."
                });
            });
    }
};

exports.findAll = (req, res, next) => {
    const title = req.query.title;

    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    Cinemahall.find(condition)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while retrieving cinema halls."
            });
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    Cinemahall.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found cinema hall with id " + id
                });
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error retrieving cinema hall with id=" + id
            });
        });
};

exports.update = (req, res, next) => {
    if (!req.body) {
        return next({
            status: 400,
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Cinemahall.findByIdAndUpdate(id, req.body)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot update cinemahall with id=${id}. Maybe cinemahall was not found!`
                });

            } else {
                res.send({ message: "cinemahall was updated successfully." });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error updating cinemahall with id=" + id
            });
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    Cinemahall.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot delete cinemahall with id=${id}. Maybe cinemahall was not found!`
                });

            } else {
                res.send({
                    message: "cinemahall was deleted successfully!"
                });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Could not delete cinemahall with id=" + id
            });
        });
};

exports.deleteAll = (req, res, next) => {
    Cinemahall.deleteMany({})
        .then((data) => {
            res.send({
                message: `${data.deletedCount} cinemahalls were deleted successfully!`
            });
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while removing all cinemahalls."
            });
        });
};