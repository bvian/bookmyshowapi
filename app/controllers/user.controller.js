const db = require("../models");
const User = db.users;

exports.create = (req, res, next) => {
    const { name, mobileNumber } = req.body;

    if (!name || !mobileNumber) {
        next({
            status: 400,
            message: "Content can not be empty! required data name, mobileNumber"
        });
        return;
    } else {

        const user = new User({
            name,
            mobileNumber
        });

        user
            .save(user)
            .then((data) => {
                res.send(data);
            })
            .catch((err) => {
                next({
                    status: 500,
                    message: err.message || "Some error occurred while creating the Users."
                });
            });
    }
};

exports.findAll = (req, res, next) => {
    const title = req.query.title;

    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};

    User.find(condition)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while retrieving users."
            });
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    User.findById(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: "Not found User with id " + id
                });
            } else {
                res.send(data);
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error retrieving User with id=" + id
            });
        });
};

exports.update = (req, res, next) => {
    if (!req.body) {
        return next({
            status: 400,
            message: "Data to update can not be empty!"
        });

    }

    const id = req.params.id;

    User.findByIdAndUpdate(id, req.body)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot update User with id=${id}. Maybe User was not found!`
                });
            } else {
                res.send({ message: "User was updated successfully." });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Error updating User with id=" + id
            });
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    User.findByIdAndRemove(id)
        .then((data) => {
            if (!data) {
                next({
                    status: 404,
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`
                });
            } else {
                res.send({
                    message: "User was deleted successfully!"
                });
            }
        })
        .catch((err) => {
            next({
                status: 500,
                message: "Could not delete User with id=" + id
            });
        });
};

exports.deleteAll = (req, res, next) => {
    User.deleteMany({})
        .then((data) => {
            res.send({
                message: `${data.deletedCount} Users were deleted successfully!`
            });
        })
        .catch((err) => {
            next({
                status: 500,
                message: err.message || "Some error occurred while removing all users."
            });
        });
};